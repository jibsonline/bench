FROM python
ARG PORT=80
WORKDIR /app
ADD . /app
RUN pip install -r requirements.txt
EXPOSE ${PORT}
CMD ["python", "run.py"]