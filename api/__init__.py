import datetime

from flask import Flask
from api.controllers import main

app = Flask(__name__)

with app.app_context():
    with open("datedb", "w") as fo:
        fo.write(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

app.register_blueprint(main, url_prefix='/')
