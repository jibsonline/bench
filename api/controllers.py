import datetime
import json

from flask import Blueprint, Response


main = Blueprint('main', __name__)


@main.route('/')
def index():
    return "Hello World"


@main.route('/status/alive')
def status():
    return ""


@main.route('/status/ready')
def ready():
    timenow=datetime.datetime.now()
    with open("datedb", "r") as fo:
        all_lines = fo.readlines()
    starttime=datetime.datetime.strptime(all_lines[0],'%Y-%m-%d %H:%M:%S')
    timediff=timenow-starttime
    uptime=timediff.total_seconds()
    if uptime < 10:
        ready="false"
        code=500
    else:
        ready="true"
        code=200
    data={}
    data["ready"]=ready
    js = json.dumps(data)
    resp = Response(js, status=code, mimetype='application/json')
    return resp
